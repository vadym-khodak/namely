.. py:module:: namely.resources.profiles
.. py:currentmodule:: namely.resources.profiles

========
profiles
========
.. autoclass:: Profile


Methods
-------

get_all
^^^^^^^

.. automethod:: Profile.get_all

get
^^^

.. automethod:: Profile.get

get_me
^^^^^^

.. automethod:: Profile.get_me

filter
^^^^^^

.. automethod:: Profile.filter

update
^^^^^^^

.. automethod:: Profile.update

create
^^^^^^^

.. automethod:: Profile.create
