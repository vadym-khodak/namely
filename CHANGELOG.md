## 0.0.3 (2021-05-28)

### Features
- make using multithreading optional
- add more docs

## 0.0.2 (2021-05-24)

### Features
- Basic API client
- profiles resource

## 0.0.1 (2021-05-24) (DELETED)

### Features
- Test release
